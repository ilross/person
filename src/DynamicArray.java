import java.util.Arrays;

public class DynamicArray {
    int[] arr = {3, 2, 7, 4, 1, 2, 3, 4};

    void addElement(int index, int element) {
        int[] temp = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
            if (i == index) {
                arr[index] = element;
            } else {
                for (int j = 0; j < arr.length; j++) {
                    temp[j] = arr[j];

                }
                temp[temp.length - 1] = element;
            }

            arr = temp;
        }
    }

    void addArray(int[] arr) {
        int[] temp = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    void addStart(int element) {
        int[] temp = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
            temp[i + 1] = arr[i];
        }
        temp[0] = element;
        arr = temp;
    }

    void addEnd(int element) {
        int[] temp = new int[arr.length + 1];
        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i];
        }
        temp[temp.length - 1] = element;
        arr = temp;
    }

    void deleteElement(int index) {
        int[] temp = new int[arr.length - 1];
        for (int i = 0; i < arr.length; i++) {
            int tempp = arr[i];
            arr[i] = arr[arr.length - 1];
            arr[arr.length - 1] = tempp;
            for (int j = 0; j < temp.length; j++) {
                temp[j] = arr[j];
            }
        }
        arr = temp;
    }

    void deleteAll() {
        int[] temp = new int[]{};
        arr = temp;
    }

    void deleteStart() {
        int[] temp = new int[arr.length - 1];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = arr[i + 1];
        }
        arr = temp;
    }

    void deleteEnd() {
        int[] temp = new int[arr.length - 1];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = arr[i];
        }
        arr = temp;
    }

    void sortMaxMin() {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
    }

    void sortMinMax() {
        for (int i = arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (arr[j] < arr[j + 1]) {
                    int tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
    }

    void revers() {
        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = temp;
        }
    }

    void halfRevers() {
        int half = arr.length / 2;
        int halfp = half + arr.length % 2;
        for (int i = 0; i < halfp; i++) {
            int t = arr[i];
            arr[i] = arr[halfp + i];
            arr[halfp + i] = t;
        }
    }

    void print() {
        System.out.println(Arrays.toString(arr));
    }

    int[] get() {
        return arr;
    }
}
