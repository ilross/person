import java.util.Arrays;

public class ArrayPerson {
    public static void main(String[] args) {
        ArrayPerson main = new ArrayPerson();
        main.addStart(new Person(3, 4, "das"));
        main.print();
        main.addStart(new Person(4, 5, "daz"));
        main.addStart(new Person(1, 4, "zx"));
        main.addEnd(new Person(5, 4, "z1"));
        main.print();
        main.addElement(new Person(5, 4, "xdsa"));
        main.print();
        main.deleteElement(4);
        main.print();
    }

    Person[] pers = new Person[]{};

    void addElement(Person person) {
        for (int i = 0; i < pers.length; i++) {
            if (pers[i].getId() == person.getId()) {
                pers[i] = person;
            } else {
                Person[] temp = new Person[pers.length + 1];
                for (int j = 0; j < pers.length; j++) {
                    temp[j] = pers[j];
                }
                temp[temp.length - 1] = person;
                pers = temp;
            }
        }
    }

    void addStart(Person person) {
        Person[] temp = new Person[pers.length + 1];
        for (int i = 0; i < temp.length - 1; i++) {
            temp[i + 1] = pers[i];
        }
        temp[0] = person;
        pers = temp;
    }

    void addEnd(Person person) {
        Person[] temp = new Person[pers.length + 1];
        for (int i = 0; i < pers.length; i++) {
            temp[i] = pers[i];
        }
        temp[temp.length - 1] = person;
        pers = temp;
    }

    void deleteElement(int id) {
        Person[] temp = new Person[pers.length - 1];
        int tempInt = 0;
        for (int i = 0; i < pers.length; i++) {
            if (pers[i].getId() == id) {
                tempInt = i;
            }
        }
        for (int i = 0; i <= 1; i++) {
            temp[i] = pers[i];
        }
        for (int i = 2; i < pers.length - 1; i++) {
            temp[i] = pers[i];
        }
        pers = temp;

    }


    void deleteAll() {
        Person[] temp = new Person[]{};
        pers = temp;
    }

    void deleteStart() {
        Person[] temp = new Person[pers.length - 1];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = pers[i + 1];
        }
        pers = temp;
    }

    void deleteEnd() {
        Person[] temp = new Person[pers.length - 1];
        for (int i = 0; i < temp.length; i++) {
            temp[i] = pers[i];
        }
        pers = temp;
    }

    void print() {
        System.out.println(Arrays.toString(pers));
    }

    Person[] getArrayPerson() {
        return pers;
    }
}
